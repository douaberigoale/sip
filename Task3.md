# Task 3 - ECOMMERCE FRAMEWORK

### Task description
Find an eCommmerce framework in which you can develop a webshop. Set up this framework and
implement/test some basic functionalities:
  - Registration and Log-in process
  - Showing dummy products to a user
  - Putting products in a cart and triggering an ordering process

### Solution

Ecommerce Platfom: Opencart 3.0.2.0
URL and Access:
  - Frontend: http://playground.irisbanciu.com/sip-eshop/
  - Admin Panel: http://playground.irisbanciu.com/sip-eshop/admin
  - Domain access: User = admin, PW = sip
  - Admin access: User = admin, PW = sip

Registration and Log-in process:
  - Frontend activity
  - User registration or login buttons are located in the header under the My Account dropdown.
The Registration is formular based.
  - Test User Account data: User: test@test.com PW: sippw
Note: Activation emails are informative only, no user action is necessary.


Showing dummy products to a user:
  - Backend catalog configuration
  - Creating new products: Catalog > Products > + (on the top right side)
 After completion of the product creation process, the products are being shown on the front end, unless otherwise specified. OpenCart provides some dummy products itself.

Putting products in a cart and triggering an ordering process:
  - Frontend activity
Note: No user registration is required for trigerring an order. Guest accounts are currently enabled.
  - Customers add products to the session cart using the icons available under each product grid or from the product page. 
  - The ordering process may be triggered from the cart or directly using the Checkout button ( top right side on any page).
  - Orders appear in user accounts and in the Admin Panel Dashboard or in the Admin Panel under Sales > Orders and may be edited.





