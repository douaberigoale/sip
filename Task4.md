# Task 4 - TOOL SUPPORT FOR THE SCRUM PROCESS

### Task description
Find tools and techniques to pursue the Scrum process. This may encompass software tools, e.g., the Atlassian
Stack, as well as general processes, e.g., handwritten backlog on post-its.

### Solution

Offline methods:

  - Kanban Post-Its Wall - simple and free, but not integrated with anything
  - Hansoft - project management tool which can also handle SCRUM - free up to nine users, but needs server
  - iceScrum - free and open source, available for all platforms
  - Scrumpy - free, only locally installable and basically created for Product Owners


Online methods:

  - JIRA - fully fledged SCRUM tool - commercial - part of the Atlassion Stack
  - GitLab Agile Delivery solution - commercial - integrated in GitLab
  - Gitlab/ GitHub Issue board - can be used for SCRUM, integrates with git processes
  - Trello - Kanban Board, fully customizable - free for small teams



